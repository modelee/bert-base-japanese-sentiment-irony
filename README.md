---
language: ja

license: cc-by-sa-4.0
---

# BERT Base Japanese for Irony 

This is a BERT Base model for sentiment analysis in Japanese additionally finetuned for automatic irony detection. 

The model was based on [bert-base-japanese-sentiment](https://huggingface.co/daigo/bert-base-japanese-sentiment), and later finetuned on a dataset containing ironic and sarcastic tweets. 


## Licenses

The finetuned model with all attached files is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/), or Creative Commons Attribution-ShareAlike 4.0 International License. 

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

## Citations

Please, cite this model using the following citation.

```
@inproceedings{dan2022bert-base-irony02,
  title={北見工業大学 テキスト情報処理研究室 ELECTRA Base 皮肉検出モデル (daigo ver.)}, 
  author={団　俊輔　and プタシンスキ ミハウ and ジェプカ ラファウ and 桝井 文人}, 
  publisher={HuggingFace}, 
  year={2022},
  url = "https://huggingface.co/kit-nlp/bert-base-japanese-sentiment-irony"
}
```
